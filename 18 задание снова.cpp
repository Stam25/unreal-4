﻿#include <iostream>
#include <stack>  
using namespace std;

int main() {
    setlocale(LC_ALL, "rus");
    stack <int> steck; 
    int i = 0;

    cout << "Введите шесть любых целых чисел: " << endl; 
    while (i != 6) {
        int a;
        cin >> a;

        steck.push(a);  
        i++;
    }

    if (steck.empty()) cout << "Стек не пуст"; 

    cout << "Верхний элемент стека: " << steck.top() << endl; 
    cout << "Давайте удалим верхний элемент " << endl;
    steck.pop();  
    cout << "А это новый верхний элемент: " << steck.top(); 
    system("pause");
    return 0;
}