﻿#include <iostream>

class Stack 
{
private:
	int* arr;
	int size;
public: 
	Stack()
	{
		arr = nullptr;
		size = 0;
	}

	void push(int a)
	{
		int* tmp;
		arr = new int[size + 1];
			tmp = arr;

		size++;
		for (int i = 0; i < size - 1; i++)
			arr[i] = tmp[i];
		arr[size - 1] = a;
			if (size > 1)
				delete[] tmp;
	}

	int pop()
	{
		size--;
		return arr[size];
	}

    void print()
	{
		int* p;
		p = arr;
		std::cout << "Array";
		for (int i = 0; i < size; i++)
		{
			std::cout << *p << ' ';
			p++;
		}
	}


};

int main()
{
	Stack V;
	V.push(1);
	V.push(2);
	V.push(100);
	V.push(5);
	V.push(4);
	V.print();
	V.pop();
};