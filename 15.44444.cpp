﻿#include <iostream> 
using namespace std;

void OddOrNot(int N, bool x)
{
    for (int i = 1; i <= N; i++)
    {
        if (x)
        {
            if (i % 2 == 0)
            {
                std::cout << i << "\n";
            }
        }
        else if (i % 2 != 0)
        {
            std::cout << i++ << "\n";
        }
    }
}

int main()
{
    setlocale(LC_ALL, ".1251");
    const int n = 20;
    int chet[n / 2];
    int i = 0, k = 0;
    cout << "Чётные числа с 0 до n:";
    while (k <= n)
    {
        chet[i] = k;
        cout << " " << chet[i];
        i++;
        k = k + 2;
        if (k < n) cout << ",";
    }
    //  cout << endl;
    //  return 0;
}