﻿#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>

int main()
{
    std::cout << "Enter your name!\n";
    std::string name ="Artem";
    std::cin >> name;

    std::cout << name.length() << "\n";
  {
    std::string A("Artem");
    char& f = A.front();
    f = 'e';
    std::cout << A << '\n'; // 
  }
 
  {
    std::string const c ("Artem");
    char const& f = c.front();
    std::cout << &f << '\n'; // 
  }

  {
      std::string m ("Artem");
      char& back = m.back();
      back = 's';
      std::cout << m << '\n'; //
  }

  {
      std::string const c("Artem");
      char const& back = c.back();
      std::cout << back << '\n'; // 'y'
  }

}