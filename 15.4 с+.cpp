﻿#include <iostream>
using namespace std;
int main()
{
    setlocale(LC_ALL, ".1251");
    const int n = 20;
    int chet[n / 2];
    int i = 0, k = 0;
    cout << "Чётные числа с 0 до n:";
    while (k <= n)
    {
        chet[i] = k;
        cout << " " << chet[i];
        i++;
        k = k + 2;
        if (k < n) cout << ",";
    }
    //  cout << endl;
    //  return 0;
}