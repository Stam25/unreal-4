﻿#include <iostream>


class Vector
{
public:
    Vector() :x(2),y(1),z(1)
    {}
    Vector(int _x,float _y,double _z) :x(_x), y(_y), z(_z)
    {}
    void xyz()
    {
        std::cout << x << " " << y << " " << z << "\n";
    }
    void Show()
    {
        std::cout << x * x + y * y << "\n";
    }
    void Gipot()
    {
        std::cout << hypot(x, y);
    }

private:
    double x = 1;
    float y = 2;
    double z = 3;
};

int main()
{
    Vector X (3, 4.4, 5.1);
    Vector Y(6, 3.3, 1.2);
    Vector Z(2, 7.6, 8.1);
}

